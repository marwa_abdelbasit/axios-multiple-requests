import { defineAbility } from "@casl/ability";

export default (user) => defineAbility((can) => {
    can('read', 'Article');

    if(user.isLogged) {
        can('update', 'Article', {authorId: user.id});
        can('update', 'Comment');
        can('update', 'Comment', {authorId: user.id});
    }
})