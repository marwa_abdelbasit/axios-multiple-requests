import axios from "axios";

const state = {
    user: localStorage.getItem('user_data') || '',
    token: localStorage.getItem('access_token') || '',
    isLogged: false
}

const getters = {
    getUser: state => JSON.parse(state.user),
    isLogged: state => state.isLogged,
    isAuthenticated: state => !!state.token,
}

const actions = {   
    loginAction: ({commit}, user) => {
        axios.post("https://skepadmin.com/api/Auth/Login", {
            email: user.email,
            password: user.password
        }).then(res => {
            if(res.data.access_token) {
                localStorage.setItem('access_token', res.data.access_token)
                localStorage.setItem('user_data', JSON.stringify(res.data.UserData))
                console.log(res.data.UserData);
                commit("logUser", true)
                window.location.replace('/')
            }
        }).catch(err => {
            console.log(err.message);
        })
    },

    logoutAction: ({commit}) => {
        localStorage.removeItem("access_token")
        localStorage.removeItem("user_data")
        delete axios.defaults.headers.common["Authorization"]
        commit("logoutUser", false)
        window.location.replace("/login")
    }
}

const mutations = {
    logUser: (state, isLogged) => {
        state.isLogged = isLogged
    },
    logoutUser: (state, isLogged) => {
        state.isLogged = isLogged
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}