import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import user from './modules/user'

Vue.use(Vuex)

let postsApi = "https://jsonplaceholder.typicode.com/posts";
let commentsApi = "https://jsonplaceholder.typicode.com/comments";
let albumsApi = "https://jsonplaceholder.typicode.com/albums";

// const requestOne = axios.get(one)
// const requestTwo = axios.get(two)
// const requestThree = axios.get(three)

export default new Vuex.Store({
  state: {
    postsGet: axios.get(postsApi),
    commentsGet: axios.get(commentsApi),
    albumsGet: axios.get(albumsApi),
    posts: [],
    displayPosts: [],
    rows: 0,
    comments: [],
    albums: []
  },
  mutations: {
    setPosts: (state, data) => {
      state.posts = data
    },
    setDisplayPosts: (state, data) => {
      state.displayPosts = data
    },
    setRows: (state, data) => {
      state.rows = data
    },
    setComments: (state, data) => {
      state.comments = data
    },
    setAlbums: (state, data) => {
      state.albums = data
    }
  },
  actions: {
    getThree: ({commit ,state}) => {
      axios.all([state.postsGet, state.commentsGet, state.albumsGet])
      .then(axios.spread((...responses) => {
        const resOne = responses[0]
        const resTwo = responses[1]
        const resThree = responses[2]
        // console.log("requestOne", resOne.data[0]);
        
        //for pagination
        const displayPosts = resOne.data.slice(0, 10)
        commit("setDisplayPosts", displayPosts)
        commit("setPosts", resOne.data);
        commit("setRows", resOne.data.length)
        // console.log("rows", resOne.data.length);
        
        // console.log("requestTwo", resTwo.data);
        commit("setComments", resTwo.data)
        // console.log("requestthree", resThree.data);
        commit("setAlbums", resThree.data)
      })).catch(errors => {
        console.log(errors.message);
      })
    },

    paginate: async ({commit, state}, {currentPage, perPage}) => {
      const start = (currentPage - 1) * perPage
      const posts = state.posts.slice(start, start + 10)
      commit("setDisplayPosts", posts)
    },

    updatePagination: ({commit, dispatch}, {posts, currentPage, perPage}) => {
      commit("setPosts", posts)
      commit("setRows", posts.length)
      dispatch("paginate", {currentPage ,perPage})
    },

    search: ({dispatch, getters}, {text}) => {
      const posts = getters.getPosts
      const values = posts.filter(val => val.title.toLowerCase().includes(text.toLowerCase()))
      dispatch("updatePagination", {
        posts: values,
        currentPage: 1,
        perPage: 10
      })
    }
  },
  getters: {
    getPosts: (state) => {
      return state.posts
    },
    getDisplayPosts: (state) => {
      return state.displayPosts
    },
    getRows: (state) => {
      return state.rows
    },
    getComments: (state) => {
      return state.comments
    },
    getAlbums: (state) => {
      return state.albums
    },
  },
  modules: {
    user
  }
})
